import React, {PureComponent} from "react";
import {Paper} from 'material-ui';
import {List, ListItem} from 'material-ui/List';
import { setActiveTheme } from '../../redux/actions/ColorThemeActions';
import { bindActionCreators } from "redux";
import {connect} from "react-redux";
import Styles from "../../styles/components/ThemesExplorer.scss";
import _ from "lodash";

@connect (
    ( store ) => ({ colorThemes: store.ColorTheme }),
    ( dispatcher ) => bindActionCreators({ setActiveTheme }, dispatcher )
)
export default class ThemesExplorer extends PureComponent {
    render() {
        const { renderItems, props } = this ;
        const { colorThemes } = props ;
        const { themes, selected } = colorThemes ;

        return <Paper className={ Styles.ThemesExplorer }>{ renderItems(themes) }</Paper>
    }
    renderItems = ( themes ) => {
        const { onItemClickHandler } = this ;
        const handler = ( theme, index ) => {
            const { id } = theme ;
            return <ListItem key={ id } data-index={ index } primaryText={ id } onClick={ onItemClickHandler }/>
        }
        return <List>{ _.map( themes, handler ) }</List>
    }
    onItemClickHandler = ( event ) => {
        const { props } = this ;
        const { setActiveTheme } = props ;
        const { currentTarget } = event ;
        const { dataset } = currentTarget ;
        const { index } = dataset ;

        setActiveTheme( index );
    }
}