import { isProductionEnvironment, Crypt } from "../utils/Common";
import _ from "lodash";

export default class LocalStorageAdaptor {
	static get( id ) {
		const json = localStorage.getItem( id ) ;
		return ! _.isNull(json) && Boolean(json.length) ? JSON.parse( isProductionEnvironment() ? Crypt( false, json ) : json ) : null ;
	}
	static set( id, data = null ) {
		const json = JSON.stringify( data )
		localStorage.setItem( id, isProductionEnvironment() ? Crypt( true, json ) : json );
		return data ;
	}
}