export const ENVIRONMENT_STATES = {
	PRODUCTION: "production",
	DEVELOPMENT: "development"
}
export const LOCALSTORAGE = {
	LS_AUTHENTICATION: "authentication",
	LS_SOCIAL_AUTHENTICATION: "social_authentication"
}
export const COLOR_THEMES = {
    DARK: {
        spacing: 1,
        fontFamily: 'Roboto, sans-serif',
        borderRadius: 0,
        palette: {
            primary1Color: "#ff9524",
            primary2Color: "#ff3600",
            primary3Color: "#000000",
            accent1Color: "#73ff2b",
            accent2Color: "#ff9e05",
            accent3Color: "#00fff0",
            textColor: "#000000",
            alternateTextColor: "#ff9524",
            canvasColor: "#ffffff",
            borderColor: "#ff9000",
            disabledColor: "#1200f0",
            pickerHeaderColor: "#00fff0",
            clockCircleColor: "#00ff00",
            shadowColor: "#000000",
        }
    },
    LIGHT: {
        spacing: 1,
        fontFamily: 'Roboto, sans-serif',
        borderRadius: 5,
        palette: {
            primary1Color: "#ffffff",
            primary2Color: "#0f0000",
            primary3Color: "#00f000",
            accent1Color: "#000f00",
            accent2Color: "#0000f0",
            accent3Color: "#00000f",
            textColor: "#ffffff",
            secondaryTextColor: "#0f0000",
            alternateTextColor: "#00f000",
            canvasColor: "#3f3f3d",
            borderColor: "#f01417",
            disabledColor: "#00000f",
            pickerHeaderColor: "#f00000",
            clockCircleColor: "#0f0000",
            shadowColor: "#00f000"
        },
    }
}



