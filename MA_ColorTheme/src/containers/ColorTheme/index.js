import React, {PureComponent} from "react";
import {getMuiTheme, MuiThemeProvider} from 'material-ui/styles';
import {connect} from "react-redux";

@connect ( ( store ) => ({ colorThemes: store.ColorTheme }) )
export default class ColorTheme extends PureComponent {
    render() {
        const { props } = this ;
        const { colorThemes } = props ;
        const { selected } = colorThemes ;
        const { children } = props ;

        return <MuiThemeProvider muiTheme={ getMuiTheme(selected) }>{ children }</MuiThemeProvider>
    }
}