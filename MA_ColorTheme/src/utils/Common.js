import _ from "lodash";
import React from "react";
import { ENVIRONMENT_STATES } from "../constants" ;
import XORCrypt from "xor-crypt";
import Base64 from "base-64";

/**
 * Генератор уникального идентификатора (аналог Lodash.uniqueId()) ;
 *
 * @param prefix
 * @returns {string}
 */
export const genUniqueId = function (prefix = null) {
	var d = new Date().getTime();
	var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
		var r = (d + Math.random() * 16) % 16 | 0;
		d = Math.floor(d / 16);
		return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
	});

	return [prefix || "", uuid].join('');
}
/**
 * @ToDo:
 * Аналог console.log(.warn/.info и т.п.) с цветовой схемой ;
 * Работает только для development окружения и НЕ РАБОТАЕТ для production
 */
export const Debug = {
	console: function (name, text, data, color) {

		if( ! isProductionEnvironment() ) {

			if (!name) name = " "; else name += " ";
			if (!text) text = " "; else text += " ";
			if (!data) data = "";

			console.log(
				"[!] " + "%c" + name + text,
				"color: " + color + ";",
				data
			);
		}
	},
	clear: console.clear,
	error: function (name, text, data) {
		const color = "#ff1f00";
		return this.console(name, text, data, color);
	},
	log: function (name, text, data) {
		const color = "#00d500";
		return this.console(name, text, data, color);
	},
	info: function (name, text, data) {
		const color = "#ff8908";
		return this.console(name, text, data, color);
	},
	warn: function (name, text, data) {
		const color = "#00bcff";
		return this.console(name, text, data, color);
	}
}
/**
 * Получение читабельного вида ошибки в axios response`е,
 * который вернул ошибку
 * @param error: Response
 */
export const parseResponseError = (error) => {
	const { response } = error;
	const { status, data } = response;
	const { error_description } = data;
	return {
		id: status,
		message: error_description
	}
}
/**
 * @ToDo:
 * Создание JSX компонента по ReactElement классу или имени DOM элемента ;
 * @param ComponentClass: ReactComponent or String ;
 * @param props: Object // options ;
 * @return {JSX}
 */
export const elementDesigner = ( ComponentClass, props={} ) => {
	let children = null ;
	if( props.children ) {
		children = [ ...props.children ] ;
		delete props.children ;
	}
	const options = _.extend({}, this.props, props ) ;

	if( ComponentClass instanceof String )
	{
		const DOMComponent = React.createFactory( ComponentClass ) ;
		return <DOMComponent { ...options } >{ children }</DOMComponent> ;

	} else return <ComponentClass { ...options } >{ children }</ComponentClass> ;
}
/**
 * @ToDo:
 * Проверка node environment ( production или development )
 * @return Boolean
 */
export const isProductionEnvironment = () => {
	return String( process.env.NODE_ENV ).toLowerCase() == ENVIRONMENT_STATES.PRODUCTION ;
}
/**
 * Шифрование строки:
 * @param method: Boolean // true - зашифровать, false - расшифровать ;
 * @param value: String // строка данных ;
 *
 * @return String
 */
export const Crypt = ( method, value ) => {

	switch( method )
	{
		case true: return Base64.encode( XORCrypt( value ) );
		case false: return XORCrypt( Base64.decode(value ) ) ;
		default: return value ;
	}

}