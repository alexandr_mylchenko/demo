import { COLOR_THEMES } from "../../constants";

const index = 0 ;
const themes = [
    { id: "Dark", theme: COLOR_THEMES.DARK },
    { id: "Light", theme: COLOR_THEMES.LIGHT }
] ;
const selected = themes[ index ].theme ;
const ColorThemeModel = { index, themes, selected } ;

export default ColorThemeModel;