import {combineReducers} from "redux";
import ColorTheme from './ColorThemeReducer';

const rootReducer = combineReducers({ ColorTheme });

export default rootReducer;