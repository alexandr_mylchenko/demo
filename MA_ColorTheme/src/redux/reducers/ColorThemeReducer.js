import Model from "../models/ColorThemeModel";
import { SET_ACTIVE_THEME } from "../actions/ColorThemeActions";

export default function ( state = { ...Model }, action ) {
    const { type } = action;
    switch (type) {
        case SET_ACTIVE_THEME: {
            const { index } = action ;
            const selected = state.themes[ index ].theme;
            return { ...state, index, selected };
        }
        default: return { ...state } ;
    };
}