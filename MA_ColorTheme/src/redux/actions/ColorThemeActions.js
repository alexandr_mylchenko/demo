export const SET_ACTIVE_THEME = 'ColorTheme.SET_ACTIVE_THEME';

export const setActiveTheme = ( index ) => {
    return ( dispatch ) => {
        dispatch({ type: SET_ACTIVE_THEME, index }) ;
    }
}